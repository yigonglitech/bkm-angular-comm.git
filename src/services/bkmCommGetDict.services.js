﻿(function () {
    'use strict';

    angular.module('bkm.library.angular.comm', ['abp'])
        .provider('dictionary', ['$provide', '$filterProvider', function ($provide, $filterProvider) {
            this.initial = function (constantVal) {
                //创建常量
                init(constantVal, $provide, $filterProvider);
            };
            this.$get = function () {
                return '';
            };
        }])
        .service('bkmCommGetDict', ['abp.services.app.sysDictionary', 'dictionaryConst', '$q', bkmCommGetDict]);

    /** @ngInject */
    function bkmCommGetDict(sysDictionaryApi, dictConst, $q) {
        var self = this;
        self.dictionary = {};
        for (var i in dictConst) {
            (function declareServiceFn(keyName) {
                const dictKey = dictConst[keyName];
                self[keyName] = function () {
                    if (!angular.isArray(self.dictionary[dictKey])) {
                        self.dictionary[dictKey] = [];
                    }
                    if (!!self.dictionary[dictKey].length) {
                        return dictKey;
                    } else {
                        self[keyName + 'Defer']();
                    }
                    return dictKey;
                };

                self[keyName + 'Defer'] = function () {
                    let deferred = $q.defer();
                    if (self.dictionary[dictKey] && !!self.dictionary[dictKey].length) {
                        deferred.resolve(self.dictionary[dictKey]);
                    } else {
                        // 从缓存中获取字典数据
                        const SysDictionaryKey = "SysDictionaryKey";
                        const localResultData = JSON.parse(bkm.util.getItem(SysDictionaryKey)) || {};
                        if (localResultData[dictKey] != null) {
                            initSysDictionary(localResultData[dictKey], dictKey, deferred);
                        } else {
                            sysDictionaryApi.getAll({
                                'type': dictKey,
                                maxResultCount: '900',
                                sorting: 'index ASC'
                            }).then((result) => {
                                const resultDataItems = result.data.items || [];
                                initSysDictionary(resultDataItems, dictKey, deferred);
                                // 缓存字典数据
                                const defaultTTL = 10 * 3600; // 过期时间为 10 小时;
                                const cacheObj = Object.assign(localResultData, { [dictKey]: resultDataItems });
                                bkm.util.setItem(SysDictionaryKey, JSON.stringify(cacheObj), defaultTTL);
                            }, (result) => {
                                deferred.reject(result.data);
                            });
                        }
                    }
                    return deferred.promise;
                };

                self['set' + keyName] = function (items) {
                    items.sort(compare('index'));
                    if (!angular.isArray(self.dictionary[dictKey])) {
                        self.dictionary[dictKey] = items;
                    } else {
                        self.dictionary[dictKey].splice(0, self.dictionary[dictKey].length);
                        Array.prototype.push.apply(self.dictionary[dictKey], items);
                    }
                };
            })(i);
        }

        function initSysDictionary(resultDataItems, dictKey, deferred) {
            if (!angular.isArray(self.dictionary[dictKey])) {
                self.dictionary[dictKey] = [];
            }
            self.dictionary[dictKey].splice(0, self.dictionary[dictKey].length);
            Array.prototype.push.apply(self.dictionary[dictKey], resultDataItems);
            deferred.resolve(self.dictionary[dictKey]);
        }

    }

    function compare(prop) {
        return function (obj1, obj2) {
            var val1 = obj1[prop];
            var val2 = obj2[prop];
            if (!isNaN(Number(val1)) && !isNaN(Number(val2))) {
                val1 = Number(val1);
                val2 = Number(val2);
            }
            if (val1 < val2) {
                return -1;
            } else if (val1 > val2) {
                return 1;
            } else {
                return 0;
            }
        };
    }

    function init(constantVal, $provide, $filterProvider) {

        //创建常量
        $provide.constant("dictionaryConst", constantVal);

        //根据常量创建 filter
        angular.forEach(constantVal, function declareFilterFn(i, key) {
            $filterProvider.register(key, ['bkmCommGetDict', function (dictSvc) {
                /**
                 * @description
                 *
                 * 根据 dictionary 中的配置，替换 input 的值
                 *
                 * @param input {string} 接收的值
                 *   error from returned function, for cases when a particular type of error is useful.
                 * @returns {string} 返回替换后的值
                 */
                return function (input, isObj, fieldName) {
                    fieldName = fieldName || 'key';
                    if (angular.isUndefined(input)) return input;
                    //从 'bkmCommGetDict' 服务中获取 dictionary 配置
                    var dicts = dictSvc.dictionary[key];
                    if (angular.isArray(dicts) && !!dicts.length) {
                        var filtered = dicts.filter((item) => item[fieldName] == input);
                        return !!isObj ? (filtered.length == 0 ? '未知' : filtered[0]) :
                            (filtered.length == 0 ? '未知' : filtered[0].name);
                    }
                    return input;
                };
            }]);
        });
    }

})();